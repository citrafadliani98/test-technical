package net.java.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import net.java.model.CitraFadliani;

@Repository
public interface CitraFadlianiRepository {
	public interface springbootRepository extends JpaRepository<CitraFadliani, Long>{
		
	}
}
