package net.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitrafadlianiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitrafadlianiApplication.class, args);
	}

}
